//use path module
const path = require('path');

//use body-parser
const bodyParser = require("body-parser");

//use express module
const express = require('express');

//use hbs view engine
const hbs = require('hbs');
const app = express();

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

//set views file
app.set('views',path.join(__dirname,'views'));

//set view engine
app.set('view engine', 'hbs');

//set public folder as static folder for static file
app.use(express.static('public'));

//route untuk halaman home
app.get('/',(req, res) => {
    //render file index.hbs
    res.render('index',{
    name : "Aris Mulyantoro"
    });
});

//route untuk halaman post
app.get('/post',(req, res) => {
    //render file form.hbs
    res.render('form');
});

//route untuk halaman home dengan parameter name
app.get('/:name',(req, res) => {
    //render file index.hbs
    res.render('index',{
    name : req.params.name
    });
});

//handle POST request
app.post('/',(req, res) => {
    var nama = req.body.nama;
    res.render('index',{
        name : nama
    });
})
app.listen(8060, () => {
    console.log('Server is running at port 8060');
});